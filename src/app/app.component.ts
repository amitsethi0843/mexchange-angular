import { Component,OnInit } from '@angular/core';
import { Location} from '@angular/common';
import { Router,Event as RouterEvent,NavigationStart,ActivatedRoute,
  NavigationEnd,NavigationCancel,NavigationError } from '@angular/router';
import {AuthService} from "./service/auth.service";
import {EventsService} from "./service/events.service"
import {CommonService} from "./service/common.service"
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'MExchange';
  logedInUserName:string;
  userToken:string;
  loading: boolean = false;
  displayerror:any;
  showHeader:boolean=true;


  constructor(private router:Router, private auth:AuthService,private customEventsService:EventsService,private location:Location,
              private commonService:CommonService) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true;
    }
    if (event instanceof NavigationEnd) {
      this.loading = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false;
    }
    if (event instanceof NavigationError) {
      this.loading = false;
    }
  }

  ngOnInit() {
    this.auth.dataChange.subscribe(data=> {
      this.userToken = data[0];
      this.logedInUserName = data[1];
      if (this.userToken) {
        this.router.navigate(['/profile'])
      }
      else {
        this.router.navigate(['/login'])
      }
    });
    this.userToken = this.auth.getUserToken();
    this.logedInUserName = this.auth.getUserName();
    if (!this.userToken && this.commonService.loginRequired(this.location.path())) {
      this.router.navigate(['/login'])
    }

    this.customEventsService.httpRequest.subscribe(data=>{
      this.loading=data
    });
    this.customEventsService.emitError.subscribe(data=>{
      this.displayerror=data;
      setTimeout(()=>{
        this.displayerror = null;
      },10000);
    });
  }


  logout() {
    this.commonService.setUrl('user/logout');
    this.commonService.getData().subscribe(
      data=> {
        if (data.response.status) {
          this.auth.removeUserData();
        }


      }
    )
    this.auth.removeUserData();
  }
}

