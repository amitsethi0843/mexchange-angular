import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation'
import { MaterializeModule } from 'angular2-materialize';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import {RegisterComponent} from './directive/component/register/register.component'
import {LoginComponent} from './directive/component/login/login.component'
import {LogoutComponent} from './directive/component/logout/logout.component'
import {ProfileComponent} from './directive/component/profile/profile.component'
import {CreateExchangeComponent} from './directive/component/exchange/create/createExchange.component'

import {CollapseOnClick} from './directive/attribute/collapseOnClick.attribute'

import {CommonService} from './service/common.service'
import {AuthService} from './service/auth.service'
import {EventsService} from './service/events.service'
import {ToastService} from './service/toast.service'
import {Routing} from "./app.routing"


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    ProfileComponent,
    CollapseOnClick,
    CreateExchangeComponent,

  ],
  imports: [
    BrowserModule,
    Routing,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    HttpModule,
    MaterializeModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey:"AIzaSyDRQTNB6DrMO-bmejwZDwc3OsObfQlsNRw",
      libraries:['places']
    }),
    BrowserAnimationsModule
  ],
  providers: [CommonService,
              AuthService,
              EventsService,
              ToastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
