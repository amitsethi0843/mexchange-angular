import {ModuleWithProviders} from "@angular/core"
import {Routes,RouterModule} from "@angular/router"
import {RegisterComponent} from './directive/component/register/register.component';
import {LoginComponent} from './directive/component/login/login.component'
import {LogoutComponent} from './directive/component/logout/logout.component'
import {ProfileComponent} from './directive/component/profile/profile.component'


const appRoutes:Routes=[
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path:'logout',component:LogoutComponent},
  {path:'profile',component:ProfileComponent},
  // {path: 'manage', component: ManageProfileComponent},
  // {path: 'verifyEmail',component:VerifyEmailComponent},
  // {path: ':userId', component: PublicUserProfileComponent},
  // {path: '**', component: PageNotFound},

];

export const Routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);
