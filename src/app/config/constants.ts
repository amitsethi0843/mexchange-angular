

import {environment} from "../../environments/environment";

export class AppConstants{
  private static UserDetails:any=
    {
    UserObject:function (token: string, username: string, firstname: string, lastname: string) {
      this.userToken = token;
      this.userName = username;
      this.firstName = firstname;
      this.lastName = lastname;
    }
  };

  private static error:any={
    CustomErrorRange:function (error:number):boolean {
      if(error >= environment.customMinError && error <= environment.customMaxError){
        return true
      }else{
        return false;
      }
    }
  }
  private static ErrorMappings:any={
    401:"Password and Email combination didn't match",
    403:"Nice try... You are not authorized to access the requested resource, please try with the different account",
    404:"Requested resource not found...Are you sure you are using thr right url and parameters",
    422:"We cannot allow you to move forward without providing all the required information",
    500:"Something serious went wrong,well blame it on us",
    DEFAULT:"Something went wrong"
};

  public static fetch():any{
    return{
      userDetails:this.UserDetails,
      errorMappings:this.ErrorMappings,
      errorConstants:this.error
    }
  }
}
