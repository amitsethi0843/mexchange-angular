
import {Component, ElementRef, Input, NgZone, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MapsAPILoader} from "@agm/core";
import {CommonService} from "../../../../service/common.service"
declare var $:any;
import {} from "@types/googlemaps"
import {CustomValidators} from "../../../../util/customValidators";
import {AuthService} from "../../../../service/auth.service"

@Component({
  selector:'create-exchange',
  templateUrl:'./createExchange.component.html'
})

export class CreateExchangeComponent implements OnInit{

  createAdForm:FormGroup;
  @Input()userDetails:any;

  @ViewChild('googleSearch') public searchElement:ElementRef;

  constructor(private commonService:CommonService,private _fb:FormBuilder,private mapsAPILoader:MapsAPILoader,private ngZone:NgZone,private authService:AuthService
  ){
    this.createAdForm=this._fb.group({
      itemName:['',Validators.required],
      itemQuantity:['1',[Validators.required,Validators.min(1)]],
      itemDescription:['',Validators.required],
      itemInWarranty:['true',Validators.required],
      itemPurchasedDate:['',Validators.required],
      adLocation:['',Validators.required],
      adLatitude:['',Validators.required],
      adLongitude:['',Validators.required],
      adCountry:['',Validators.required],
      adState:['',Validators.required],
      adCity:[],
      adPinCode:[],
      adContact:['',Validators.required],
      adEmail:['',[Validators.required,CustomValidators.validateEmail]]
    })
  }

  ngOnInit(){

    this.commonService.setUrl("profile/userDetails?username="+this.authService.getUserName()+"&parameters=contact");
    this.commonService.getData().subscribe(
      data=> {
        if (data.status) {
          this.createAdForm.patchValue({adContact:data.response.userDetailsList[0],adEmail:this.authService.getUserName()})
        }
      }
    )

    this.mapsAPILoader.load().then(
      ()=>{
        //or $('#id)[0] instead of this.searchElement.nativeElement
        let autocomplete=new google.maps.places.Autocomplete(this.searchElement.nativeElement,{
          types:["address"]
        });

        autocomplete.addListener("place_changed",()=>{
          this.ngZone.run(()=>{
            let place:google.maps.places.PlaceResult=autocomplete.getPlace();
            if(place.geometry===undefined|| place.geometry==null){
              return;
            }
            else{
              this.createAdForm.patchValue({adLatitude:place.geometry.location.lat(),adLongitude:place.geometry.location.lng(),
                                          adLocation:place.formatted_address});
              let country,city,state,zipcode;
              for(let addressComponent of place.address_components){
                if(addressComponent.types.find(type=>type=="country")){
                  country=addressComponent.long_name;
                }
                else if(addressComponent.types.find(type=>type=="postal_code")){
                  zipcode=addressComponent.long_name;
                }
                else if(addressComponent.types.find(type=>type=="administrative_area_level_1")){
                  state=addressComponent.long_name;
                }
                else if(addressComponent.types.find(type=>type=="locality")){
                  city=addressComponent.long_name;
                }
                this.createAdForm.patchValue({adCountry:country,adPinCode:zipcode,adState:state,adCity:city});
              }
              console.log(this.createAdForm.value)
            }
          })
        })
      }
    )
  }

  createAdd(){
    if(this.createAdForm.valid){
      this.commonService.setData(this.createAdForm.value);
      this.commonService.setUrl("exchange/create");
      console.log(this.commonService.getUrl());
      this.commonService.postData().subscribe(
        data=> {
          console.log(data)
        })
    }
    else {
      console.log(this.createAdForm.errors)
    }
  }

}
