
import {Component} from '@angular/core'
import { FormGroup,FormControl,FormBuilder,Validators } from '@angular/forms';
import {AuthService} from "../../../service/auth.service";
import {CommonService} from "../../../service/common.service"
import { Router } from '@angular/router';
import {EventsService} from "../../../service/events.service"
import {CustomValidators} from "../../../util/customValidators";

@Component({
  templateUrl: 'login.component.html',
  providers: [CommonService],
})

export class LoginComponent {


  loginForm:FormGroup;

  constructor(private commonService:CommonService,private _fb:FormBuilder,
              private auth:AuthService,private customEventService:EventsService,private router:Router){

    if(auth.getUserToken()){
      this.router.navigate(['profile'])
    }
    this.loginForm = this._fb.group({
      username: ['', [Validators.required,CustomValidators.validateEmail]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  login() {
    if (this.loginForm.valid && this.loginForm.value.username && this.loginForm.value.password) {
      this.commonService.setData(this.loginForm.value);
      this.commonService.setUrl("user/login");
      this.commonService.postData().subscribe(
        data=> {
          if (data.response.token && data.response.username) {
            this.auth.setUserData(data.response.token,data.response.username,data.response.firstName,data.response.lastName);
          }
        },
        error=>{
          if(error=="400" || error=="401"){
            // this.customEventService.displayError("User doesn't exist")
          }

        },
        ()=>console.log("finished")
      )
    }

  }
}
