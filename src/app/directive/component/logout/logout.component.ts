import {Component} from '@angular/core'
import {AuthService} from "../../../service/auth.service";
import { Router} from "@angular/router"

@Component({
  templateUrl: './logout.component.html',
})

export class LogoutComponent{
  constructor(private auth:AuthService,private router:Router){
    this.auth.removeUserData();
    this.router.navigate(['/login']);
  }
}
