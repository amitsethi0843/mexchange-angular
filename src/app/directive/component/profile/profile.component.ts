import {Component,OnInit} from '@angular/core'
import {CommonService} from "../../../service/common.service"
import {AuthService} from "../../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: 'profile.component.html',
})

export class ProfileComponent implements OnInit{

  userdata:Object;
  profileView:string="RECENTEXCHANGES";

  constructor(private commonService:CommonService,private auth:AuthService, private router:Router){
    let userData=this.auth.getUserData();
    if (userData) {
      this.userdata=userData;
    }
    else{
      this.router.navigate(['/login'])
    }
  }

  ngOnInit(): void {
    // this.commonService.setUrl("user/profile/"+this.userdata['userName']);
    // this.commonService.getData().subscribe(
    //   data=>{
    //
    //   }
    // )

  }
}
