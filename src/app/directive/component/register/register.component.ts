import {Component} from "@angular/core"
import {CommonService} from "../../../service/common.service"
import {AuthService} from "../../../service/auth.service"
import {AppSettings} from "../../../config/appSettings"
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {CustomValidators} from '../../../util/customValidators'

import { Router } from '@angular/router';

@Component({
  templateUrl: 'register.component.html',
  providers: [CommonService],
})

export class RegisterComponent {
  registrationForm:FormGroup;
  showError:boolean;
  minPassLength:number;

  constructor(private commonService:CommonService, private auth:AuthService, private _fb:FormBuilder, private router:Router) {
    this.minPassLength=AppSettings.fetch().appServer.passwordLength;
    if(auth.getUserToken()){
      this.router.navigate(['/'])
    }
    this.registrationForm = this._fb.group({
      username: ['', [Validators.required, CustomValidators.validateEmail]],
      password: ['', [Validators.required, Validators.minLength(this.minPassLength)]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      retypePassword: ['', [Validators.required, Validators.minLength(this.minPassLength)]]
    })
  }

  register(){
    if (this.registrationForm.valid) {
      this.commonService.setData(
        this.registrationForm.value
      );
      this.commonService.setUrl('user/register');
      this.commonService.postData().subscribe(
        data=> {
          if (data.response.token && data.response.username) {
            this.auth.setUserData(data.response.token,data.response.username,data.response.firstName,data.response.lastName);
          }
        },
        error=>console.log(JSON.stringify(error))
      )
    }
    else {
      this.showError = true;
    }
  }
}
