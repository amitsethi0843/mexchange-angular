
import {Injectable,EventEmitter} from "@angular/core";
import {AppSettings} from "../config/appSettings"
import {first} from "rxjs/operator/first";
import {AppConstants} from "../config/constants";
@Injectable()
export class AuthService {
    username:string;
    usertoken:string;
    //dataChange:Observable<any>;
    dataChange:EventEmitter<any>=new EventEmitter();
    //userTokenChange:EventEmitter ;
    //constructor() {
    //    this.dataChange=new EventEmitter();
    //    this.dataChange = new Observable((observer:Observer)=> {
    //        this.dataChange = observer;
    //    });
    //}

    appUrl:string = AppSettings.fetch().appServer.url;
    googleMapKey:string = "AIzaSyDuM08PJs00SK8Xyclb0DKaWz0_sMFtqwI";

    getGoogleMapKey() {
        return this.googleMapKey
    }

    getUserToken() {
      if(this.usertoken){
        return this.usertoken
      }else {
        let userData = this.getUserData();
        if (userData) {
          return userData['userToken'];
        }
      }
      return null;
    }

    getUserName() {
      if(this.username){
        return this.username
      }else{
        let userData=this.getUserData();
        if(userData){
          return userData['userName'];
        }
      }
      return null;
    }


    getUserData():Object{
      let userobj=localStorage.getItem("userDetails");
      if(userobj){
        userobj=JSON.parse(userobj);
      }
      return userobj?userobj:null;

    }
    setUserData(token:string, username:string,firstname:string,lastname:string) {
      let userobj=AppConstants.fetch().userDetails.UserObject;

      localStorage.setItem('userDetails',JSON.stringify(new userobj(token,username,firstname,lastname)));
      this.usertoken = token;
      this.username = username;
      this.dataChange.emit([this.usertoken,this.username]);
    }


    removeUserData() {
      localStorage.removeItem('userDetails');
        this.username = null;
        this.usertoken = null;
        this.dataChange.emit([this.usertoken,this.username]);
    }
}
