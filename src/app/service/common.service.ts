import {Injectable} from "@angular/core";
import {Http,Headers,RequestMethod,RequestOptions,Request,Response,URLSearchParams} from "@angular/http"
import {AuthService} from "./auth.service"
import 'rxjs/add/operator/map'
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Rx';
import {EventsService} from "./events.service"


@Injectable()
export class CommonService {
  url:string;
  data:any;
  requestoptions: RequestOptions;
  urlSearchParams:URLSearchParams;
  noLoader:boolean=false;
  constructor(private http:Http, private auth:AuthService,private customEvents:EventsService) {
  }
  setHeader(headers:Headers) {
    var token:string = this.auth.getUserToken();
    headers.append('Content-Type', 'application/json');
    if (token && token != "") {
      headers.append('Authorization', 'Bearer '+token);
    }
  }
  setMultipartHeader(headers:Headers){
    var token:string = this.auth.getUserToken();
    //headers.append("enctype","multipart/form-data");
    headers.append('method', 'GET');

    if (token && token != "") {
      headers.append('Authorization', 'Bearer '+token);
    }
  }

  postMultipart(input:FormData) {
    var headers = new Headers();
    this.setMultipartHeader(headers);
    this.requestoptions = new RequestOptions({
      headers: headers
    });
    return this.http.post(this.url,input,this.requestoptions)
      .map((response) => {
        return response.json();
      })
  }

  getMultipart(){
    var headers = new Headers();
    this.setMultipartHeader(headers);
    //headers.append('responseType', ResponseContentType.Blob);
    this.requestoptions = new RequestOptions({
      headers: headers,
      responseType:3,
    });

    return this.http.get(this.url,this.requestoptions)
      .map(res => {
          //var arrayBufferView = new Uint8Array( res.arrayBuffer() );
          return new Blob( [res.blob()], { type: "application/octet-stream"} );
      })
  }


  setUrl(url:string) {
    this.url = environment.serverUrl + url
  }

  getUrl() {
    return this.url;
  }

  setData(data:any) {
    this.data = data;
  }

  setNoLoader(status:boolean){
    this.noLoader=status;
  }

  getData() {
    this.customEvents.changeHttpRequestStatus(true);
    var headers = new Headers();
    this.setHeader(headers);
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Get,
      url: this.url,
      search:this.urlSearchParams?this.urlSearchParams:null,
      headers: headers,
    });
    return this.http.request(new Request(this.requestoptions))
      .map((response) => {
        var jsonResponse=response.json();
        if(jsonResponse.status && jsonResponse.errorCode >= 200 && jsonResponse.errorCode < 300){
          this.customEvents.changeHttpRequestStatus(false);
          return jsonResponse;
        }else{
          this.customEvents.changeHttpRequestStatus(false);
          this.customEvents.displayError(jsonResponse.message)
        }
      })
      .catch((error: any) => {
        if (error.status === 500) {
          console.log("error------------------500");
        }
        else if (error.status === 400) {
          console.log("error------------------400");
        }
        else if(error.status === 401){
        }
        this.customEvents.changeHttpRequestStatus(false);
        this.customEvents.displayError(error['_body']);
        return Observable.throw(error['_body'])
      });
  }


    postData() {
    this.customEvents.changeHttpRequestStatus(this.noLoader?false:true);
    var headers = new Headers();
    this.setHeader(headers);
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Post,
      url: this.url,
      headers: headers,
      body: JSON.stringify(this.data)
    });
    //var requestdata = this.convertRequestParams();
    return this.http.request(new Request(this.requestoptions))
      .map((response) => {
        var jsonResponse=response.json();
        if((jsonResponse.status && jsonResponse.errorCode >= 200 && jsonResponse.errorCode < 300) || jsonResponse.token){
          this.customEvents.changeHttpRequestStatus(false);
          return jsonResponse;
        }else{
          this.customEvents.changeHttpRequestStatus(false);
          this.customEvents.displayError(jsonResponse.message)
        }
      }).catch((error: any) => {

        if (error.status < 500 && error.status >= 400) {
          console.log("error------------------400");
        }
        else if (error.status >= 500) {
          console.log("error------------------500");
        }
        this.customEvents.changeHttpRequestStatus(false);
        this.customEvents.displayError(error['_body']);
        return Observable.throw(error.status)
      });
  }

  deleteData() {
    this.customEvents.changeHttpRequestStatus(this.noLoader?false:true);
    var headers = new Headers();
    this.setHeader(headers);
    this.requestoptions = new RequestOptions({
      method: RequestMethod.Delete,
      url: this.url,
      headers: headers,
      body: JSON.stringify(this.data)
    });
    //var requestdata = this.convertRequestParams();
    return this.http.request(new Request(this.requestoptions))
      .map((response) => {
        this.customEvents.changeHttpRequestStatus(false);
        return response.json();
        //return [{ status: res.status, json: res }]
      }).catch((error: any) => {
        if (error.status < 500 && error.status >= 400) {
          console.log("error------------------400");
        }
        else if (error.status >= 500) {
          console.log("error------------------500");
        }
        this.customEvents.changeHttpRequestStatus(false);
        return Observable.throw(error.status)
      });
  }

  loginRequired(urlStr:string):boolean{
    let required=false;
    for (let url of environment.loginRequired) {
      if(url===urlStr){
        required=true;
        break;
      }
    }
    return required
  };








  //uploadFile(file: File){
  //  var headers = new Headers();
  //  this.setHeader(headers);
  //  return new Promise((resolve, reject) => {
  //    var formData: any = new FormData();
  //    var xhr = new XMLHttpRequest();
  //    formData.append("file", file, file.name);
  //    xhr.onreadystatechange = function () {
  //      if (xhr.readyState == 4) {
  //        if (xhr.status == 200) {
  //          resolve(JSON.parse(xhr.response));
  //        } else {
  //          reject(xhr.response);
  //        }
  //      }
  //    }
  //    this.setxhrHeader(xhr);
  //    xhr.open("POST", this.url, true);
  //    xhr.send(formData);
  //  })
  //
  //}


  //convertRequestParams() {
  //  var requestParam = "";
  //  var totalParams = Object.keys(this.data).length;
  //  for (var i in this.data) {
  //    var lastKey = Object.keys(this.data).indexOf(i) == (totalParams - 1);
  //    requestParam += (i + "=" + this.data[i] + (lastKey ? "" : "&"))
  //  }
  //  return requestParam ? (requestParam) : ""
  //}
}
