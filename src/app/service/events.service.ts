import {Injectable,EventEmitter} from "@angular/core";
import {AppConstants} from "../config/constants";

@Injectable()
export class EventsService {

  headerVisible:boolean = true;
  httpRequestStatus:boolean=false;
  headerVisibilityChange:EventEmitter<any> = new EventEmitter();
  httpRequest:EventEmitter<any> = new EventEmitter();
  emitError:EventEmitter<any> = new EventEmitter();

  displayError(error:string){
    let errorData=JSON.parse(error);
    if(!AppConstants.fetch().errorConstants.CustomErrorRange(Number(errorData['errorCode']))){
      let errorMessage=AppConstants.fetch().errorMappings[errorData['errorCode']];
      this.emitError.emit(errorMessage?errorMessage:AppConstants.fetch().errorMappings.DEFAULT)
    }

  }

  changeHeaderVisibility(visible:boolean){
    this.headerVisible=visible;
    this.headerVisibilityChange.emit(this.headerVisible)
  }

  changeHttpRequestStatus(status:boolean){
    this.httpRequestStatus=status;
    this.httpRequest.emit(this.httpRequestStatus)
  }


}
