export const environment = {
  production: true,
  serverUrl: "http://api.trioq.com/api/",
  refreshTokenUrl:"http://api.mexchange.com/oauth/access_token",
  loginRequired:['/profile'],
  googleCloudBucket:'dappled-genre-5740',
  customMinError:1001,
  customMaxError:9999
};
