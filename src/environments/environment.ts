// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  serverUrl: "http://localhost:8080/api/v1/",
  refreshTokenUrl:"http://localhost:8080/oauth/access_token",
  loginRequired:['/profile'],
  googleCloudBucket:'dappled-genre-5740',
  customMinError:1001,
  customMaxError:9999
};
